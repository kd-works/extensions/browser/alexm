'use strict';

let data = {
	debug: false,
	//endpoint: 'http://localhost:3000/v1/alexm',
	endpoint: 'https://api.kasai.moe/v1/alexm',
	notif: false,
	links: {
		twitch: 'https://www.twitch.tv/alexmytb_',
		youtube: {
			gaming: true,
			live: 'https://{{ytg}}youtube.com/c/AlexMAW/live',
			video: 'https://{{ytg}}youtube.com/watch?v='
		}
	},
	video: {
		content: {
			author: '',
			id: '',
			pubDate: '',
			title: '',
			desc: '',
			thumb: ''
		}
	},
	live: {
		platform: '',
		content: {
			author: '',
			pubDate: '',
			title: '',
			game: '',
			desc: '',
			thumb: ''
		}
	}
};

if (data.links.youtube.gaming) {
	data.links.youtube.live = data.links.youtube.live.replace('{{ytg}}', 'gaming.');
	data.links.youtube.video = data.links.youtube.video.replace('{{ytg}}', 'gaming.');
} else {
	data.links.youtube.live = data.links.youtube.live.replace('{{ytg}}', '');
	data.links.youtube.video = data.links.youtube.video.replace('{{ytg}}', '');
}

class JSP {
	constructor() {
		this.debug = data.debug;
	}

	init(name) {
		if (this.debug) console.log('Initialisation...', name);
	}

	checkStatus() {
		if (this.debug) console.log('Checking status!');
	}

	dl(url) {
		if (this.debug) console.log('Téléchargement de l\'url :', url);
		return fetch(url)
			.then(this.debug ? console.log('Téléchargement terminé !') : null)
			.then(res => res)
			.catch(err => err);
	}

	sendNotif(status) {
		if (this.debug) console.log('Envoi de la Notification :', status);
	}

	openLink(link) {
		if (this.debug) console.log('Ouverture du lien :', link);

		chrome.tabs.create({ url: link });
	}
}

// LIVE
class Live extends JSP {
	constructor() {
		super();

		this.init();
	}

	init() {
		super.init('Live');

		this.checkStatus();
		setInterval(this.checkStatus, 3e4);
	}

	checkStatus() {
		super.checkStatus();

		super.dl(`${data.endpoint}/live`)
			.then(res => res.json())
			.then(async res => {
				if (res === undefined || res === null) return this.sendNotif('error');
				if (res.status === 'online') {
					if (data.debug) console.log('Live Found!');

					data.live.content.author = res.stream.author.name;
					data.live.content.pubDate = res.stream.publishedAt;
					data.live.content.title = res.stream.title;
					data.live.content.game = res.stream.game;
					data.live.content.desc = res.stream.description;
					data.live.content.thumb = res.stream.thumbnail;

					chrome.browserAction.setTitle({ title: `${data.live.content.author} est en live !` });
					this.sendNotif(true);
				} else if (res.status === 'offline') {
					if (data.debug) console.log('No Stream');
				} else {
					if (debug) console.log('An error occured!');

					this.sendNotif('error');
				}
			})
			.catch(err => err);
	}

	sendNotif(status) {
		super.sendNotif(status);

		if (status) {
			if (data.notif) return;
			chrome.browserAction.setIcon({ path: './assets/img/icons/32-on.png' });

			chrome.storage.sync.get({
				liveNotif: true,
				liveSound: true,
				liveSoundChoice: 0,
				livePersist: false
			}, async items => {
				if (!items.liveNotif) return;
				chrome.notifications.create('live', {
					type: 'basic',
					title: `${data.live.content.author} est en live !`,
					message: `${data.live.content.game} | ${data.live.content.title}`,
					iconUrl: chrome.extension.getURL('assets/img/icons/128-on.png'),
					//imageUrl: await stream.thumb.then(res => URL.createObjectURL(res)),
					priority: 2,
					requireInteraction: items.livePersist,
					buttons: [
						{
							title: 'YouTube',
							iconUrl: chrome.extension.getURL('assets/img/notifs/youtube.png')
						},
						{
							title: 'Twitch',
							iconUrl: chrome.extension.getURL('assets/img/notifs/twitch.png')
						}
					]
				}, notifId => {
					chrome.notifications.onButtonClicked.addListener((notifIdClick, index) => {
						if (notifId === notifIdClick) {
							index === 0 ? super.openLink(data.links.youtube) : super.openLink(data.links.twitch);
						}
					});
				});

				if (items.liveSoundChoice) {
					const audio = new Audio(`assets/sounds/${items.liveSoundChoice}.mp3`);
					audio.volume = 0.5;
					audio.play();
				}
			});

			data.notif = true;
		} else if (data.notif) {
			chrome.browserAction.setIcon({ path: './assets/img/icons/32-off.png' });
			chrome.notifications.clear('live');
			data.notif = false;
		}
	}
}

// VIDEO
class Vidéo extends JSP {
	constructor() {
		super();

		this.init();
	}

	init() {
		super.init('Vidéo');

		this.checkStatus();
		setInterval(this.checkStatus, 18e5);
	}

	checkStatus() {
		super.checkStatus();

		const date = new Date();
		date.setDate(date.getDate() - 30);

		super.dl(`${data.endpoint}/video?pubDate=${date.toISOString()}`)
			.then(res => res.json())
			.then(async res => {
				if (res === undefined || res === null) return this.sendNotif('error');
				if (res.result) {
					if (data.debug) console.log('New Vidéo Found!');

					data.video.content.author = res.result.author.name;
					data.video.content.id = res.result.id;
					data.video.content.title = res.result.title;
					data.video.content.desc = res.result.description;
					data.video.content.pubDate = res.result.publishedAt;
					data.video.content.thumb = super.dl(`http://cors-anywhere.herokuapp.com/${res.result.thumbnail}`).then(res => res.blob()).catch(err => err); // res.result.thumbnail;

					this.sendNotif(true);
				} else if (!res.result) {
					if (data.debug) console.log('No New Vidéo');
				} else {
					if (debug) console.log('An error occured!');

					this.sendNotif('error');
				}
			})
			.catch(err => err);
	}

	sendNotif(status) {
		super.sendNotif(status);

		if (status) {
			chrome.storage.sync.get({
				videoNotif: true,
				videoSound: true,
				videoSoundChoice: 0,
				videoPersist: false
			}, async items => {
				if (!items.videoNotif) return;
				chrome.notifications.create('video', {
					type: 'image',
					iconUrl: chrome.extension.getURL('assets/img/icons/128-on.png'),
					title: `Nouvelle vidéo d'${data.video.content.author} !`,
					message: data.video.content.title,
					contextMessage: data.video.content.desc,
					priority: 2,
					imageUrl: await data.video.content.thumb.then(res => URL.createObjectURL(res)),
					requireInteraction: items.videoPersist
				}, notifId => chrome.notifications.onClicked.addListener(notifId => {
					chrome.notifications.clear(notifId);
					super.openLink(`${data.links.youtube.video}${data.video.content.id}`);
				}));

				if (items.liveSoundChoice) {
					const audio = new Audio(`assets/sounds/${items.liveSoundChoice}.mp3`);
					audio.volume = 0.5;
					audio.play();
				}
			});

		} else if (data.notified) {
			chrome.browserAction.setIcon({ path: './assets/img/icons/32-off.png' });
			chrome.notifications.clear('video');
		}
	}
}

new Live();
new Vidéo();
